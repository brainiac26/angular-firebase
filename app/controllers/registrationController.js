"use strict";

angular.module('myApp')
    .controller('RegistrationController', ['$scope', 'AuthService',
        function ($scope, AuthService) {

            $scope.error = '';

            $scope.loginUser = function(user) {
                $scope.error = '';
                AuthService.login(user).catch(function(error) {
                    $scope.error = error;
                })
            };

            $scope.register = function (user) {
                $scope.error = '';
                AuthService.register(user).catch(function(error) {
                    $scope.error = error;
                });
            };

        }]);
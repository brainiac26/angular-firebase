"use strict";

angular.module('myApp')
    .controller('MeetingsController', ['$scope', '$rootScope', '$firebaseAuth', '$firebaseArray', 'FIREBASE_URL',
        function($scope, $rootScope, $firebaseAuth, $firebaseArray, FIREBASE_URL) {

            var ref = new Firebase(FIREBASE_URL),
                auth = $firebaseAuth(ref);

            auth.$onAuth(function(authUser) {
                if (authUser) {
                    var meetingsRef = new Firebase(FIREBASE_URL + '/users/' +
                        $rootScope.currentUser.$id + '/meetings');
                    var meetingsInfo = $firebaseArray(meetingsRef);
                    $scope.meetings = meetingsInfo;
                    meetingsInfo.$loaded().then(function() {
                        $rootScope.meetingsCount = meetingsInfo.length;
                    });

                    meetingsInfo.$watch(function(val) {
                        $rootScope.meetingsCount = meetingsInfo.length;
                    });

                    $scope.addMeeting = function(meetingName) {
                        meetingsInfo.$add({
                            name: meetingName,
                            date: Firebase.ServerValue.TIMESTAMP
                        }).then(function() {
                            $scope.meetingName = '';
                        })
                    };
                    $scope.deleteMeeting = function(key) {
                        meetingsInfo.$remove(key);
                    }
                }
            })
    }]);

"use strict";

angular.module('myApp')
.controller('CheckinsController', ['$scope', '$firebaseObject', '$firebaseArray', 'FIREBASE_URL', '$routeParams', '$location',
    function($scope, $firebaseObject, $firebaseArray, FIREBASE_URL, $routeParams, $location) {

        $scope.meeting = $routeParams.mId;
        $scope.user = $routeParams.uId;
        var ref = new Firebase(FIREBASE_URL + '/users/' + $scope.user + '/meetings/' + $scope.meeting + '/checkins');

        var checkins = $firebaseArray(ref);
        $scope.checkins = checkins;

        $scope.addCheckin = function(checkin) {
            var myData = {
                firstName: checkin.firstName,
                lastName: checkin.lastName,
                email: checkin.email,
                date: Firebase.ServerValue.TIMESTAMP
            };
            checkins.$add(myData).then(function() {
                $location.path('#/checkins/' + $scope.user + '/' + $scope.meeting + '/checkinsList');
            });
        };

        $scope.deleteCheckin = function(key) {
            checkins.$remove(key);
        }


}]);
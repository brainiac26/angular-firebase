"use strict";

angular.module('myApp')
    .controller('NavController', ['$scope', '$location', 'AuthService',
        function ($scope, $location, AuthService) {

            $scope.isActive = function (route) {
                return route == $location.path();
            };

            $scope.logout = function () {
                AuthService.logout();
            };

        }]);

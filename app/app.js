'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute', 'ngAnimate', 'firebase'])
    .constant('FIREBASE_URL', 'https://angdata26-app.firebaseio.com')
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'views/login.html',
            controller: 'RegistrationController'
        });
        $routeProvider.when('/register', {
            templateUrl: 'views/register.html',
            controller: 'RegistrationController'
        });
        $routeProvider.when('/checkins/:uId/:mId', {
            templateUrl: 'views/checkins.html',
            controller: 'CheckinsController'
        });
        $routeProvider.when('/checkins/:uId/:mId/checkinsList', {
            templateUrl: 'views/checkinsList.html',
            controller: 'CheckinsController'
        });
        $routeProvider.when('/meetings', {
            templateUrl: 'views/meetings.html',
            controller: 'MeetingsController',
            resolve: {
                authenticated: function (AuthService) {
                    return AuthService.requireAuth();
                }
            }
        });

        $routeProvider.otherwise({redirectTo: '/login'});
    }]);




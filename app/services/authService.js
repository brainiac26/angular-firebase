"use strict";

angular.module('myApp')
.factory('AuthService', ['$location', '$rootScope', '$q', '$firebaseAuth', '$firebaseObject', 'FIREBASE_URL',
    function ($location, $rootScope, $q, $firebaseAuth, $firebaseObject, FIREBASE_URL) {
        var ref = new Firebase(FIREBASE_URL),
            auth = $firebaseAuth(ref),
            usersRef = ref.child('users');

        auth.$onAuth(function(authUser) {
           if (authUser) {
               var userRef = usersRef.child(authUser.uid);
               var userObj = $firebaseObject(userRef);
               $rootScope.currentUser = userObj;
           } else {
               $rootScope.currentUser = null;
           }
        });
        return {
            requireAuth: requireAuth,
            login: login,
            logout: logout,
            register: register
        };

        function requireAuth() {
            return auth.$requireAuth();
        }

        function login(user) {
            var deferred = $q.defer();
            auth.$authWithPassword({
                email: user.email,
                password: user.password
            }).then(function(regUser) {
                $location.path('/meetings');
                deferred.resolve();
            }).catch(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        function logout() {
            auth.$unauth();
        }

        function register(user) {
            var deferred = $q.defer();
            auth.$createUser({
                email: user.email,
                password: user.password
            }).then(function(regUser) {
                usersRef.child(regUser.uid).set({
                    date: Firebase.ServerValue.TIMESTAMP,
                    regUser: regUser.uid,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email
                });
                login(user);
                deferred.resolve();
            }).catch(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
}]);